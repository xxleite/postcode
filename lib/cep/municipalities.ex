defmodule CEP.Municipalities do
  @moduledoc """
  Documentation for `CEP.Municipalities`.
  """

  alias CEP.{Utils, Municipalities}

  defstruct has_municipalities?: false,
            stats?:              false,
            states:              %{},
            municipalities:      [],
            timeout:             900,
            cache:               nil

  @municipalities_url "https://sidra.ibge.gov.br/Territorio/Unidades?nivel=6"

  @doc """
  Get municipalities
  """

  def get(
    %CEP{all?: all?, municipalities?: municipalities?} = options
  ) when all? and not municipalities? do
    options
  end

  @doc """
  Get municipalities
  """

  def get(%CEP{
    all?: all?,
    municipalities?: municipalities?,
    stats?: stats?,
    cache_exists?: cache_exists?,
    cache: cache,
    timeout: timeout
  } = options) when all? or municipalities? do
    sections = if cache_exists?, do: Utils.list_sections(cache), else: []

    %{ states: states, municipalities: municipalities } =
      %Municipalities{
        timeout: timeout,
        cache: cache,
        stats?: stats?,
        has_municipalities?: municipalities? and Enum.member?(sections, "municipalities")
      }
      |> load_municipalities_from_cache
      |> get_municipalities
      |> stats

    %CEP{ options | states: states, municipalities: municipalities, municipalities?: true }
  end

  def get_municipalities(
    %Municipalities{has_municipalities?: has_municipalities?} = struct
  ) when has_municipalities? do
    struct
  end

  def get_municipalities(
    %Municipalities{has_municipalities?: has_municipalities?, cache: cache, timeout: timeout} = struct
  ) when not has_municipalities? do
    case HTTPoison.get(@municipalities_url, [], timeout: 15_000, recv_timeout: timeout * 1_000) do
      {:ok, %{status_code: 200, body: body}} ->
        Utils.write_cache(body, cache, :municipalities)
        |> Poison.decode
        |> parse_municipalities(struct)

      {:ok, %{status_code: status}} ->
        exit {:error, "something went wrong, http code ##{status}"}

      {:error, %{reason: reason}} ->
        exit {:error, "something else went really wrong, reason: #{reason}"}
    end
  end

  def load_municipalities_from_cache(
    %Municipalities{has_municipalities?: has_municipalities?} = struct
  ) when not has_municipalities? do
    struct
  end

  def load_municipalities_from_cache(
    %Municipalities{has_municipalities?: has_municipalities?, cache: cache} = struct
  ) when has_municipalities? do
    Utils.read_cache(cache, :municipalities) |> parse_municipalities(struct)
  end

  def parse_municipalities a, b, c, d \\ []
  def parse_municipalities(
    [municipality | m_tail], [state | s_tail], %{municipalities: municipalities} = struct, s_cache
  ) do
    parse_municipalities(
      m_tail,
      s_tail,
      %Municipalities{struct | municipalities: ["#{municipality}_#{state}" | municipalities] },
      [ %{state: state, municipality: municipality} | s_cache ]
    ) 
  end

  def parse_municipalities [], [], %{municipalities: municipalities} = struct, s_cache do
    %Municipalities{struct |
      municipalities:      municipalities,
      states:              Enum.group_by(s_cache, & &1[:state]),
      has_municipalities?: true
    }
  end

  def parse_municipalities {:ok, %{"Nivel"=>%{"Id"=>6}, "Nomes"=>names, "SiglasUF"=>ufs}}, struct do
    parse_municipalities names, ufs, struct
  end

  def stats(%{stats?: stats?} = struct) when not stats?, do: struct
  def stats(%{states: states, municipalities: municipalities, stats?: stats?} = struct) when stats? do
    IO.puts "Total municipalities: #{Enum.count(municipalities)}"
    municipalities_by_state Map.keys(states), struct
  end

  def municipalities_by_state a, b, c \\ []
  def municipalities_by_state [head | tail], %{states: states} = struct, final do
    municipalities_by_state tail, struct, [~s("#{head}": #{Enum.count(states[head])},\t) | final]
  end
  def municipalities_by_state [], struct, final do
    Enum.chunk_every(final, 4)
    |> Enum.map(& Enum.join(&1))
    |> Enum.join("\n")
    |> IO.puts

    struct
  end
end
