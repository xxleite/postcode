defmodule CEP.Utils do

  @moduledoc """
  Documentation for Utils.
  """

  @doc """
  List all sections in temp file
  """

  def list_sections filename do
    case File.read filename do
      {:error, _flag} ->
        exit({:error, "failed to read '#{filename}'"})

      {:ok, content} ->
        Regex.scan(~r/#### (\w+) /, content)
        |> List.flatten
        |> Enum.drop_every(2)
    end
  end

  @doc """
  Read section from tempfile
  """

  def read_cache filename, section do
    case File.read filename do
      {:error, _flag} ->
        exit({:error, "failed to read '#{filename}'"})

      {:ok, content} ->
        result = case String.split content, ~r(#### #{section}.*) do
          [_first, last] ->
            case String.split last, "####" do
              [first | _final] -> first
              [first] -> first
            end

          [_first] -> nil
        end

        if result, do: String.split(result, "\n", trim: true), else: []
    end
  end

  @doc """
  Write (or append) section to tempfile
  """

  def write_cache content, filename, section do
    if not File.exists? filename do
      case File.touch filename do
        {:error, _reason} -> exit({:error, "failed to touch '#{filename}'"})
        :ok -> nil
      end
    end

    {prepend, append} = File.read!(filename)
    |> String.split(~r(#### #{section}.*))
    |> case do
      [first, last] ->
        case String.split last, "####" do
          [_ | final] ->
            {String.trim(first), Enum.join(final, "####") |> String.trim}

          [final] ->
            {String.trim(first), final}
        end

      [body] -> {body, ""}
    end

    File.write(filename, String.trim(
      "#{prepend}\n#### #{section} #{System.system_time(:second)}\n#{content}\n####\n#{append}"
    ))

    content
  end

  @doc """
  Parse string into integer and return tuple
  """

  def parse_int string do
    case Integer.parse string do
      :error   -> {:error, "not a number"}
      {int, _} -> {:ok, int}
    end
  end
end
