defmodule CEP do
  @moduledoc """
  Documentation for `CEP`.
  """

  defstruct municipalities?: false,
            neighbourhoods?: false,
            all?:            false,
            cache_exists?:   false,
            stats?:          false,
            states:          %{},
            municipalities:  [],
            neighbourhoods:  %{},
            timeout:         900,
            cache:           nil


  @doc """
  Main method called by command line

  ./cep -f temp  -t timeout  -v
  """

  def main(args \\ []) do
    %{
      flags:   %{
        municipalities: municipalities?, neighbourhoods: neighbourhoods?, verbosity: debug, stats: stats
      },
      options: %{
        cache: cache, timeout: timeout
      }
    } = Optimus.new!(
      name: "cep",
      description: "Utility for populating neighboorhods, municipalities, states and CEP of Brazil",
      allow_unknown_args: false,
      parse_double_dash: true,
      flags: [
        neighbourhoods: [short: "-n", help: "get neighbourhoods"],
        municipalities: [short: "-m", help: "get municipalities"],
        stats: [short: "-s", help: "get statistics"],
        verbosity: [short: "-v", help: "verbosity level", multiple: true]
      ],
      options: [
        cache: [
          value_name: "/path/to/filename",
          short: "-c",
          long: "--cache",
          help: "load data from cache",
          parser: fn filename ->
            case File.exists? filename do
              true  -> {:ok, filename}
              false -> {:error, "file '#{filename}' not found"}
            end
          end,
          default: fn ->
            {:ok, tmp_path} = Temp.path(%{prefix: "cep"})
            tmp_path
          end
        ],
        timeout: [
          value_name: "900",
          short: "-t",
          long: "--timeout",
          help: "IBGE connection timeout",
          parser: &Utils.parse_int/1,
          default: 900
        ]
      ]
    )
    |> Optimus.parse!(args)

    Municipalities.get(%CEP{
      all?:            !municipalities? and !neighbourhoods?,
      municipalities?: municipalities?,
      cache_exists?:   File.exists?(cache),
      stats?:          stats,
      timeout:         timeout,
      cache:           cache,
      states:          %{},
      municipalities:  []
    })

    #    Places.get(%Xenophon{
    #  all?:         !areas? and !isolated? and !places?,
    #  areas?:       areas?,
    #  places?:      places?,
    #  isolated?:    isolated?,
    #  temp_exists?: File.exists?(temp),
    #  area:         area,
    #  timeout:      timeout,
    #  kind:         Utils.parse_kind_of_places(kind),
    #  temp:         temp,
    #  debug:        debug
    #})
    #|> Areas.get
    #|> Isolations.process
  end


end
