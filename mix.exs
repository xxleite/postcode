defmodule CEP.MixProject do
  use Mix.Project

  def project do
    [
      app:             :cep,
      version:         "0.1.0",
      elixir:          "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps:            deps,
      escript:         escript
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [extra_applications: [:logger, :httpoison]]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [httpoison: "~> 1.7", poison: "~> 4.0", optimus: "~> 0.1.11", temp: "~> 0.4.7"]
  end

  defp escript do
    [main_module: CEP]
  end
end
